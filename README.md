# mth-search-bar (BETA VERSION)

Research bar with an optionnal fuzzy search mode.

## Types

| Name         | Value                                             |
| ------------ | ------------------------------------------------- |
| `SearchData` | `{ searchInput: string, searchType: SearchType }` |
| `SearchType` | `"FUZZY" \| "EXACT"`                              |

## Attributes

| Name        | Description                                                | Type      | Default |
| ----------- | ---------------------------------------------------------- | --------- | ------- |
| `delayTime` | number of milliseconds to wait before sending the event    | `number`  | —       |
| `simple?`   | if the search is only an exact search or if it can be both | `boolean` | `false` |

## Events

| Name     | Description                                 | Type of `$event`                                                    |
| -------- | ------------------------------------------- | ------------------------------------------------------------- |
| `search` | triggered `delayTime` ms after the user typed in the input | `{searchInput: string, searchType: SearchType}` |

## Use example

This web component requires [Vuetify](https://vuetifyjs.com/en/).

```html
<MthSearchBar :delayTime="300" @search="print($event)" />

<script setup lang="ts">
import { MthSearchBar } from "@metabohub/mth-search-bar"; //import component
import "@metabohub/mth-search-bar/dist/style.css"; // import style
</script>
```

## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Type-Check, Compile and Minify for Production

```sh
npm run build
```

### Run Unit Tests with [Vitest](https://vitest.dev/)

```sh
npm run test:unit
```

Check the coverage of the unit tests :
```sh
npm run coverage
```

### Lint with [ESLint](https://eslint.org/)

```sh
npm run lint
```

### View documentation

```sh
npm run storybook
```

## CICD pipeline

### Tests

```
test:
  image: node:latest
  stage: test
  before_script:
    - npm install
  script:
    - npm run test:unit
```

This runs the unit tests defined in `src/components/__tests__/MthSearchBar.spec.ts`

### Deploy

```
.publish:
  stage: deploy
  before_script:
    - apt-get update && apt-get install -y git default-jre
    - npm install
    - npm run build
    - npm run chromatic
```

This builds the component as an npm package, and publish the story in chromatic, a website to view the storybook.

