import type { Meta, StoryObj } from "@storybook/vue3";
import { action } from "@storybook/addon-actions";
import MthSearchBar from "../components/MthSearchBar.vue";

const meta: Meta<typeof MthSearchBar> = {
  title: "MthSearchBar",
  component: MthSearchBar,
  tags: ["autodocs"], // automatic documentation
};

export default meta;
type Story = StoryObj<typeof MthSearchBar>;

export const Default: Story = {
  render: (args) => ({
    components: { MthSearchBar },
    setup() {
      return {
        ...args,
        search: action("search"),
      };
    },
    template: "<MthSearchBar @search='search' :delayTime='delayTime' />",
  }),
  args: {
    delayTime: 500,
  },
};

export const SimpleSearch: Story = {
  render: (args) => ({
    components: { MthSearchBar },
    setup() {
      return {
        ...args,
        search: action("search"),
      };
    },
    template:
      "<MthSearchBar @search='search' :delayTime='delayTime' :simple='true' />",
  }),
  args: {
    delayTime: 500,
  },
};
