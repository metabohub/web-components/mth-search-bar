import type { SearchType } from "@/types/SearchType";

export type SearchData = {
  searchInput: string;
  searchType: SearchType;
};
