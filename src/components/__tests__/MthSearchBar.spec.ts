import { describe, expect, test, vi } from "vitest";
import { mount } from "@vue/test-utils";
import { createVuetify } from "vuetify";

import MthSearchBar from "@/components/MthSearchBar.vue";

const vuetify = createVuetify();
function searchBarWrapper(delayTime: number) {
  return mount(MthSearchBar, {
    global: {
      plugins: [vuetify],
    },
    propsData: { delayTime: delayTime },
  });
}

vi.useFakeTimers();

describe("MthSearchBar.vue", () => {
  test("should emit event on keyup after delay time", async () => {
    // mount wrapper
    const delayTime: number = 300;
    const wrapper = searchBarWrapper(delayTime);
    // trigger keyup
    await wrapper.find("input").trigger("keyup");
    // expect 'search' to have been emitted AFTER delay time passed as props
    expect(wrapper.emitted().search).toBeFalsy();
    vi.advanceTimersByTime(delayTime);
    expect(wrapper.emitted().search).toBeTruthy();
  });
  test("should swap search type when swap button is clicked", async () => {
    // mount wrapper
    const delayTime: number = 300;
    const wrapper = searchBarWrapper(delayTime);
    // at the beginning the placeholder should say 'Search'
    let label = await wrapper.find(".v-field-label");
    expect(label.text()).toBe("Search");
    // trigger click on button
    await wrapper.find(".mdi-swap-horizontal").trigger("click");
    // placeholder should now say 'Fuzzy search'
    label = await wrapper.find(".v-field-label");
    expect(label.text()).toBe("Fuzzy search");
    // trigger click on button
    await wrapper.find(".mdi-swap-horizontal").trigger("click");
    // placeholder should be back to 'Search'
    label = await wrapper.find(".v-field-label");
    expect(label.text()).toBe("Search");
  });
  test("clicking on the reset button should clear the input filed", async () => {
    // mount wrapper
    const delayTime: number = 300;
    const wrapper = searchBarWrapper(delayTime);
    // write in input field
    const textInput = wrapper.find('input[type="text"]');
    await textInput.setValue("bla");
    expect(
      (wrapper.find('input[type="text"]').element as HTMLInputElement).value,
    ).toBe("bla");
    // click on 'clear' button
    await wrapper.find(".mdi-close-circle").trigger("click");
    // input field should be empty
    expect(
      (wrapper.find('input[type="text"]').element as HTMLInputElement).value,
    ).toBe("");
  });
});
